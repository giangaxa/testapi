﻿namespace TestAPI_ViewModel.ViewModels
{
    public class CategoryVM
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
