﻿namespace TestAPI_ViewModel.ViewModels
{
    public class ProductVM
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
