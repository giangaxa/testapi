﻿using AutoMapper;
using TestAPI.Entities;
using TestAPI_ViewModel.ViewModels;

namespace TestAPI.Configurations
{
    public class MapperConfig:Profile
    {
        public MapperConfig()
        {
            CreateMap<Product, ProductVM>().ReverseMap();
            CreateMap<Category, CategoryVM>().ReverseMap();
        }
    }
}
