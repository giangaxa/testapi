﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using TestAPI.Entities;

namespace TestAPI.Configurations
{
    public class ProductConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(c => c.Id);
            builder.HasOne<Category>(c => c.Category).WithMany(c => c.Products).HasForeignKey(c => c.CategoryId);
        }
    }
}
