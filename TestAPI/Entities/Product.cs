﻿namespace TestAPI.Entities
{
    public class Product:BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
