﻿using System;
using TestAPI.Enum;

namespace TestAPI.Entities
{
    public class BaseEntity
    {
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public Status Status { get; set; }
    }
}
