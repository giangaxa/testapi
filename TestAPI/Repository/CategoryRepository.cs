﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestAPI.DbContext;
using TestAPI.Entities;
using TestAPI.Enum;
using TestAPI.IRepository;

namespace TestAPI.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly XaGaShopDbContext _context;
        public CategoryRepository(XaGaShopDbContext context)
        {
            _context = context;
        }
        public bool Create(Category category)
        {
            if (category == null) return false;
            category.CreateAt = DateTime.Now;
            category.UpdateAt = DateTime.Now;
            category.Status = Status.Active;

            _context.Categories.Add(category);
            _context.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            Category category = _context.Categories.FirstOrDefault(c => c.Id == id);
            if (category == null) return false;
            category.Status = Status.Delete;

            _context.Categories.Update(category);
            _context.SaveChanges();
            return true;
        }

        public List<Category> GetAll()
        {
            return _context.Categories.Where(c=>c.Status==Status.Active).ToList();
        }

        public Category GetById(int id)
        {
            return _context.Categories.FirstOrDefault(c => c.Id == id && c.Status==Status.Active);
        }

        public bool Update(Category category)
        {
            if (category == null) return false;
            category.UpdateAt = DateTime.Now;

            _context.Categories.Update(category);
            _context.SaveChanges();
            return true;
        }
    }
}
