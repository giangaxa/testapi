﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestAPI.DbContext;
using TestAPI.Entities;
using TestAPI.Enum;
using TestAPI.IRepository;

namespace TestAPI.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly XaGaShopDbContext _context;
        public ProductRepository(XaGaShopDbContext context)
        {
            _context = context;
        }
        public bool Create(Product product)
        {
            if (product == null) return false;
            product.CreateAt = DateTime.Now;
            product.UpdateAt = DateTime.Now;
            product.Status = Status.Active;

            _context.Products.Add(product);
            _context.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            Product product = _context.Products.FirstOrDefault(c => c.Id==id);
            if (product == null) return false;
            product.Status = Status.Delete;

            _context.Products.Update(product);
            _context.SaveChanges();
            return true;
        }

        public List<Product> GetAll()
        {
            return _context.Products.Where(c=>c.Status==Status.Active).ToList();
        }

        public Product GetById(int id)
        {
            return _context.Products.FirstOrDefault(c => c.Id == id && c.Status==Status.Active);
        }

        public bool Update(Product product)
        {
            if (product == null) return false;
            product.UpdateAt = DateTime.Now;

            _context.Products.Update(product);
            _context.SaveChanges();
            return true;
        }
    }
}
