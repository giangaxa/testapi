﻿using System.Collections.Generic;
using TestAPI.Entities;

namespace TestAPI.IRepository
{
    public interface IProductRepository
    {
        bool Create(Product product);
        List<Product> GetAll();
        bool Update(Product product);
        bool Delete(int id);
        Product GetById(int id);
    }
}
