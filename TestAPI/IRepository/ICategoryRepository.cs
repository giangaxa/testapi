﻿using System.Collections.Generic;
using TestAPI.Entities;

namespace TestAPI.IRepository
{
    public interface ICategoryRepository
    {
        bool Create(Category category);
        List<Category> GetAll();
        Category GetById(int id);
        bool Delete(int id);
        bool Update(Category category);
    }
}
