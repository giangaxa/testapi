﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TestAPI.Entities;
using TestAPI.IRepository;
using TestAPI_ViewModel.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryRepository _category;
        private readonly IMapper mapper;

        public CategoryController(ICategoryRepository category,IMapper mapper)
        {
            this.mapper = mapper;
            _category = category;
        }
        // GET: api/<CategoryConteoller>
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = mapper.Map<List<CategoryVM>>(_category.GetAll());

            return Ok(model);
        }

        // GET api/<CategoryConteoller>/5
        [HttpGet]
        [Route("id")]
        public IActionResult GetById(int id)
        {
            var model = mapper.Map<CategoryVM>(_category.GetById(id));

            return Ok(model);
        }

        // POST api/<CategoryConteoller>
        [HttpPost]
        public IActionResult Create(CategoryVM categoryVM)
        {
            var created = _category.Create(mapper.Map<Category>(categoryVM));

            return CreatedAtAction(nameof(Create), created);
        }

        // PUT api/<CategoryConteoller>/5
        [HttpPut]
        public IActionResult Update(CategoryVM categoryVM)
        {
            _category.Update(mapper.Map<Category>(categoryVM));

            return Ok();
        }

        // DELETE api/<CategoryConteoller>/5
       [HttpPut]
       [Route("id")]
       public IActionResult Delete(int id)
        {
            _category.Delete(id);

            return Ok();
        }
    }
}
