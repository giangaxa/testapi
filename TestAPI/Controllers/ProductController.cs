﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TestAPI.Entities;
using TestAPI.IRepository;
using TestAPI_ViewModel.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _product;
        private readonly IMapper mapper;
        private readonly ICategoryRepository _category;

        public ProductController(IProductRepository product, IMapper mapper, ICategoryRepository category)
        {
            _product = product;
            this.mapper = mapper;
            _category = category;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = mapper.Map<List<ProductVM>>(_product.GetAll());
            var test = _category.GetAll();
            foreach (var x in model)
            {
                foreach (var ca in _category.GetAll())
                {
                    if (x.CategoryId==ca.Id)
                    {
                        x.CategoryName = ca.Name;
                    }
                }
            }

            return Ok(model);
        }

        // GET api/<ProductController>/5
       [HttpGet]
       [Route("id")]
       public IActionResult GetById(int id)
        {
            var model = mapper.Map<ProductVM>(_product.GetById(id));
            foreach (var x in _category.GetAll())
            {
                if (model.CategoryId==x.Id)
                {
                    model.CategoryName = x.Name;
                }
            }

            return Ok(model);
        }

        // POST api/<ProductController>
       [HttpPost]
       public IActionResult Create(ProductVM productVM)
        {
            Product model = mapper.Map<Product>(productVM);
            model.Category = null;
            _product.Create(model);

            return Ok();
        }

        // PUT api/<ProductController>/5
        [HttpPut]
        public IActionResult Update(ProductVM productVM)
        {
            Product model = mapper.Map<Product>(productVM);
            model.Category = null;
            _product.Update(model);

            return Ok();
        }

        // DELETE api/<ProductController>/5
       [HttpPut]
       [Route("id")]
       public IActionResult Delete(int id)
        {
            _product.Delete(id);

            return Ok();
        }
    }
}
